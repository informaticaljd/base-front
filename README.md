# BaseFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.2.

## Install dependencies
Run `npm install` to execute

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Generate Mock Data
Run `npm run generate-fake-data` to execute generator data

## Run Mock Api
Run `npm run server` to execute api

## It is allowed to use
- PrimeNG components
- Lodash utility library
- i18N ngx-translate system
- CSS utility library featuring various helpers such as a grid system, flexbox, spacing, elevation and more PrimeFlex
- Icon libraries: PrimeIcons, FontAwesome, Material Icons Design and LineIcons
- Style sheet in SCSS formats
- Definition for environments: local, dev, test, pre and prod
- Definition of shared module for generic components
- Component mode development

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
