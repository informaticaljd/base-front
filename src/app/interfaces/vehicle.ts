export interface Vehicle {
  id: number;
  model: string;
  type: string;
  fuel: string;
  color: string;
  imageUrl: string;
}
