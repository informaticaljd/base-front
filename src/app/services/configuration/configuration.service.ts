import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as local } from '@env/environment.local';
import { environment as dev } from '@env/environment.dev';
import { environment as test } from '@env/environment.test';
import { environment as pre } from '@env/environment.pre';
import { environment as prod } from '@env/environment.prod';
import { EnvironmentInterface } from '@env/environment.interface';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  private _config: EnvironmentInterface | undefined;

  constructor(
    private http: HttpClient
  ) {
  }

  load(): Promise<any> {
    const headers = new HttpHeaders().set('Cache-Control', 'no-cache')
      .set('Pragma', 'no-cache')
      .set('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT')
      .set('If-Modified-Since', '0');
    return this.http.get<any>('configuration/configuration.json', { headers: headers }).toPromise().then((r: any) => {
      this.config = r.env === 'pro' ? prod : r.env === 'pre' ? pre : r.env === 'test' ? test : r.env === 'dev' ? dev : local;
    });
  }

  /**
   * Getter config
   * @return {EnvironmentInterface | undefined}
   */
  public get config(): EnvironmentInterface | undefined {
    return this._config;
  }

  /**
   * Setter config
   * @param {EnvironmentInterface | undefined} value
   */
  public set config(value: EnvironmentInterface | undefined) {
    this._config = value;
  }

}
