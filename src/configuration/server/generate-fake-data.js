const faker = require('faker');
const database = {
  vehicles: []
};

for (let i = 1; i <= 107; i++) {
  database.vehicles.push({
    id: i,
    model: faker.vehicle.model(),
    type: faker.vehicle.type(),
    fuel: faker.vehicle.fuel(),
    color: faker.vehicle.color(),
    imageUrl: faker.image.transport()
  });
}

console.log(JSON.stringify(database));
