import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { Vehicle } from '../../interfaces/vehicle';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  // Base url
  baseurl = 'localhost:3000';

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {

      console.error(`${ operation } failed: ${ error.message }`);
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  get(): Observable<Vehicle[]> {
    const headers = new HttpHeaders({
      accept: 'application/json'
    });

    return this.httpClient.get<Vehicle[]>(this.baseurl + '/vehicles', {
      headers
    })
      .pipe(
        catchError(this.handleError<Vehicle[]>('get', [])),
        retry(1),
        map(value => value)
      );
  }
}
